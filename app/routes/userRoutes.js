var express = require('express');
var config = require('../../config');
var User = require('../models/user');
var router = express.Router();
var redirect_uri = 'http://127.0.0.1:8080/api/users/callback';
var client_id = '0b73b553fc76d5b96c3f7648b45a62049768dc41f52ad44c6634d9bddf25d957';
var client_secret = 'INTRODUCE RIGHT SECRET HERE';

router.route('/')

    // Create user
    // Accessed at POST http:localhost:xxxx/api/users
    .post(function (req, res) {

        var newUser = new User();

        newUser.name = req.body.name;
        newUser.password = req.body.password;
        newUser.save(function (err) {
            if (err)
                return res.status(500).send('Error creating the user!');

            res.json({
                message: 'User created!',
            });
        })

    })

    .get(function (req, res) {
        User.find(function (err, users) {
            if (err)
                res.send(err);

            console.log(JSON.stringify(users));
            res.json(users);
        });
    });

router.route('/authorize')

    .get(function (req, res) {
        /*
        var request = require('request');
        request({
            method: 'GET',
            url: 'https://api.trakt.tv/oauth/authorize?response_type=code&client_id=' + client_id + '&redirect_uri=' + redirect_uri,
            headers: {
                'Content-Type': 'application/json'
            }
        }, function (error, response, body) {
            console.log('Authorizing.....')
        });
        */

        res.redirect('https://api.trakt.tv/oauth/authorize?response_type=code&client_id=' + client_id + '&redirect_uri=' + redirect_uri);
    });

router.route('/callback')

    // Accessed at GET http:localhost:xxxx/api/callback
    .get(function (req, res) {
        var aux = req.url.split("=");
        //  console.log(aux[1]); // Valor do Code recebido

        var request = require('request');

        request({
            method: 'POST',
            url: 'https://api.trakt.tv/oauth/token',
            headers: {
                'Content-Type': 'application/json'
            },
            body: "{  \"code\": \"" + aux[1] + "\",  \"client_id\": \"" + client_id + "\",  \"client_secret\": \"" + client_secret + "\",  \"redirect_uri\": \"" + redirect_uri + "\",  \"grant_type\": \"authorization_code\"}"
        }, function (error, response, body) {
            console.log('Status:', response.statusCode);
            // console.log('Headers:', JSON.stringify(response.headers));
            console.log('Response:', body);
        });
    });


module.exports = router;