// server.js

// BASE SETUP
// =============================================================================

// call the packages we need
var express = require('express'); // call express
var app = express(); // define our app using express
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

var config = require('./config'); // get our config file
var User = require('./app/models/user');
var userRoutes = require('./app/routes/UserRoutes');

var cors = require('cors');
app.use(cors());

// configure app to use bodyParser()
// this will let us get the data from a POST
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.use('/api/users', userRoutes);

var port = process.env.PORT || 8080; // set our port

// CONNECT TO OUR DATABASE
mongoose.connect(config.database, {
    useMongoClient: true
}); // connect to our database

// ROUTES FOR OUR API
// =============================================================================
var router = express.Router(); // get an instance of the express Router

// secret variable
app.set('superSecret', config.secret);

// middleware to use for all requests
router.use(function (req, res, next) {
    // do logging
    console.log('Something is happening.');
    next(); // make sure we go to the next routes and don't stop here
});

// test route to make sure everything is working (accessed at GET http://localhost:8080/api)
router.get('/', function (req, res) {
    res.json({
        message: 'hooray! welcome to our api!'
    });
});

// REGISTER OUR ROUTES -------------------------------
// all of our routes will be prefixed with /api
app.use('/api', router);

// START THE SERVER
// =============================================================================
app.listen(port);
console.log('Magic happens on port ' + port);